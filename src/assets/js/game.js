var game = new Phaser.Game(640, 480, Phaser.AUTO, 'phaser-game', null, false, false);

var PhaserGame = function() {

};

var ground = [];
var player;
var enemies = [];
var leftPipeSprite, rightPipeSprite;
var score = 0,
  scoreObject;

PhaserGame.prototype = {
  preload: function() {
    this.load.image('background', 'assets/img/background-full.png');
    this.load.image('pipe', 'assets/img/pipe.png');
    this.load.image('ground', 'assets/img/ground.png');
    this.load.spritesheet('enemy', 'assets/img/enemy.png', 16, 26, 3, 0);
    this.load.spritesheet('player', 'assets/img/player.png', 17, 28, 5, 0);
  },
  create: function() {
    game.add.image(0, 0, 'background');
    var fontStyle = {
      font: '16px Emulogic',
      fill: '#fff',
    };

    scoreObject = game.add.text(8, 8, score + ' Points', fontStyle);
    scoreObject.setShadow(2, 4, '#000', 6, 8);


    // Physics
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.physics.arcade.gravity.y = 100;


    // Adding Pipes
    leftPipeSprite = game.add.sprite(8, 352 + 32, 'pipe');
    leftPipeSprite.scale.setTo(2, 2);
    game.physics.enable(leftPipeSprite, Phaser.Physics.ARCADE);
    leftPipeSprite.body.immovable = true;
    leftPipeSprite.body.allowGravity = false;

    rightPipeSprite = game.add.sprite(568, 352 + 32, 'pipe');
    rightPipeSprite.scale.setTo(2, 2);
    game.physics.arcade.enableBody(rightPipeSprite);
    rightPipeSprite.body.immovable = true;
    rightPipeSprite.body.allowGravity = false;


    // Ground
    for (var i = 0; i < 20; i++) {
      ground.push(game.add.sprite(32 * i, 448, 'ground'));
      ground[i].scale.setTo(2, 2);

      game.physics.enable(ground[i], Phaser.Physics.ARCADE);
      ground[i].body.immovable = true;
      ground[i].body.allowGravity = false;
    }


    // Initialize enemy spawning
    if (enemies.length === 0) {
      spawnEnemy();
    }



    // Player
    player = game.add.sprite(game.world.centerX, 50, 'player');
    player.scale.setTo(2, 2);
    player.anchor.setTo(0.5, 0.5);

    player.animations.add('left', [0, 1], 15, true);
    player.animations.add('right', [0, 1], 15, true);
    player.animations.add('jump', [2], 1, true);
    player.animations.add('fall', [3], 1, true);
    player.animations.add('dead', [4], 1, true);

    game.physics.enable(player, Phaser.Physics.ARCADE);
    player.body.collideWorldBounds = true;
    player.body.gravity.y = 1000;
    player.body.maxVelocity.y = 500;

    // Player status: 0 alive, 1 dead
    player.status = 0;
  },
  update: function() {
    // Update score
    scoreObject.setText(score + (score === 1 ? ' Point' : ' Points'));


    // Collisions
    if (player.status === 0) {
      ground.forEach(function(tile) {
        game.physics.arcade.collide(player, tile);
      });
    } else {
      player.body.collideWorldBounds = false;
    }

    game.physics.arcade.collide(player, leftPipeSprite);
    game.physics.arcade.collide(player, rightPipeSprite);


    // Enemiy logic
    enemies.forEach(function(enemy) {
			if (enemy.status <= 3) {
				game.physics.arcade.collide(enemy, ground);

				// Pipe Collisions
				[leftPipeSprite, rightPipeSprite].forEach(function(pipe) {
					// Event handler
					game.physics.arcade.collide(enemy, pipe, function() {
						if (enemy.facing === 'left') {
							enemy.facing = 'right';
							enemy.body.velocity.x = 40;
							enemy.animations.play('right');
            	enemy.scale.x = -2;
						} else {
							enemy.facing = 'left';
							enemy.body.velocity.x = -40;
							enemy.animations.play('left');
            	enemy.scale.x = 2;
						}
					});
				});
			}

      switch (enemy.status) {
        // Spawn enemy
        case 0:
          var spawnLeftPipe = Math.floor(Math.random() * 2);
          var velocityFactor = Math.random() + 0.5;
          if (spawnLeftPipe) {
            enemy.position.x = 38;
            enemy.position.y = 390;
            enemy.body.velocity.x = 200 * velocityFactor;
          } else {
            enemy.position.x = 610;
            enemy.position.y = 390
            enemy.body.velocity.x = -200 * velocityFactor;
          }

          enemy.body.gravity.y = 1000;
          enemy.body.maxVelocity = 500;
          enemy.body.velocity.y = -750;

          enemy.status += 1;
          enemy.facing = 'left';
          break;
        case 1:
          // Is landed on the ground?
          if (enemy.body.position.y === 396) {
            enemy.body.velocity.x = 0;

            enemy.status += 1;

            window.setTimeout(function() {
              if (enemy.body.position.x > player.body.position.x) {
                enemy.body.velocity.x = -40;
                enemy.animations.play('left');
              } else {
                enemy.body.velocity.x = 40;
                enemy.animations.play('right');
                enemy.scale.x = -2;
              }

              enemy.status += 1;
            }, 1000);
          }
          break;
        case 3:
          // -- Idle mode

          break;
      }
    });


    // Player movement
    var cursors = game.input.keyboard.createCursorKeys();
    player.body.velocity.x = 0;

    // Player enemy collision
    enemies.forEach(function(enemy, index) {
      // Is the enemy alive?
      if (enemy.status === 3 && player.status === 0) {
        game.physics.arcade.collide(enemy, player, function() {
          // Did the player land on top of the enemie?
					var distance = enemy.body.position.y - player.body.position.y;
          if (distance === 56) {
            // Dying animation
            enemy.status = 4;
            enemy.animations.play('dead');
            enemy.body.velocity.y = -200;
            enemy.body.velocity.x = 0;


            // Throw player in the air
            player.body.velocity.y = -550;

            // Increase score
            score += 1;


            window.setTimeout(function() {
              enemy.kill();
              //enemies.splice(index, 1);
            }, 1000);
          } else {
            player.status = 1;
            player.animations.play('dead');
            player.body.velocity.y = -400;
            setTimeout(function() {
              player.kill();
            }, 2000);
          }
        }, function() {
          // Kill the player when he is inside of an enemy
          if (player.body.position.y === 392) {
            player.status = 1;
            player.animations.play('dead');
            player.body.velocity.y = -400;
            setTimeout(function() {
              player.kill();
            }, 2000);
          }
        });
      }
    });

    // Is the player alive
    if (player.status === 0) {
      if (cursors.left.isDown) {
        //  Move to the left
        player.body.velocity.x = -250;
        player.animations.play('left');

        // Face left
        player.scale.x = -2;
      } else if (cursors.right.isDown) {
        //  Move to the right
        player.body.velocity.x = 250;
        player.animations.play('right');

        // Face right
        player.scale.x = 2;
      } else {
        //  Stand still
        player.frame = 0;
      }

      //  Allow the player to jump if they are touching the ground.
      if (cursors.up.isDown && player.body.touching.down) {
        player.body.velocity.y = -550;
      }

      // Flying animation
      // Is the player in the air
      if (!player.body.touching.down) {
        // Is the player falling
        player.animations.stop();
        if (player.body.velocity.y <= 1) {
          player.animations.play('jump');
        } else {
          player.animations.play('fall');
        }
      }
    }
  }
};

function spawnEnemy() {
  var enemy = game.add.sprite(game.world.centerX + 20, 40, 'enemy');
  enemy.scale.setTo(2, 2);
  enemy.anchor.setTo(0.5, 0.5);
  enemy.immovable = true;

  enemy.animations.add('left', [1, 2], 6, true);
  enemy.animations.add('right', [1, 2], 6, true);
  enemy.animations.add('air', [0]);
  enemy.animations.add('dead', [0]);

  /*
  0: Not drawn to screen
  1: Drawn to screen and launched
  2: Land on the ground
  3: Out of shell and walking around
  4: Dying
  */
  enemy.status = 0;

  game.physics.enable(enemy, Phaser.Physics.ARCADE);

  enemies.push(enemy);

  // Random timer
  var timer = Math.random() * 2 + 0.5;
  setTimeout(function() {
    spawnEnemy();
  }, timer * 1000);
}

game.state.add('Game', PhaserGame, true);
